SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[latinizer].[SafeWords]') AND type in (N'U'))
BEGIN
CREATE TABLE [latinizer].[SafeWords](
	[Word] [varchar](50) NOT NULL,
	[AddedDate] [datetime2](7) NOT NULL,
	[Inactive] [bit] NOT NULL,
 CONSTRAINT [PK_SafeWords] PRIMARY KEY CLUSTERED 
(
	[Word] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[latinizer].[DF_SafeWords_AddedDate]') AND type = 'D')
BEGIN
ALTER TABLE [latinizer].[SafeWords] ADD  CONSTRAINT [DF_SafeWords_AddedDate]  DEFAULT (sysutcdatetime()) FOR [AddedDate]
END
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[latinizer].[DF_SafeWords_Inactive]') AND type = 'D')
BEGIN
ALTER TABLE [latinizer].[SafeWords] ADD  CONSTRAINT [DF_SafeWords_Inactive]  DEFAULT ((0)) FOR [Inactive]
END
GO
