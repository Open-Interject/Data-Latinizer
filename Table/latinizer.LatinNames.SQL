SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[latinizer].[LatinNames]') AND type in (N'U'))
BEGIN
CREATE TABLE [latinizer].[LatinNames](
	[NameType] [varchar](10) NOT NULL,
	[NameText] [nvarchar](50) NOT NULL,
	[Gender] [varchar](1) NOT NULL,
	[AddedDate] [datetime2](7) NOT NULL,
	[Inactive] [bit] NOT NULL,
 CONSTRAINT [PK_LatinNames] PRIMARY KEY CLUSTERED 
(
	[NameType] ASC,
	[NameText] ASC,
	[Gender] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[latinizer].[DF_LatinNames_AddedDate]') AND type = 'D')
BEGIN
ALTER TABLE [latinizer].[LatinNames] ADD  CONSTRAINT [DF_LatinNames_AddedDate]  DEFAULT (sysutcdatetime()) FOR [AddedDate]
END
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[latinizer].[DF_LatinNames_Inactive]') AND type = 'D')
BEGIN
ALTER TABLE [latinizer].[LatinNames] ADD  CONSTRAINT [DF_LatinNames_Inactive]  DEFAULT ((0)) FOR [Inactive]
END
GO
