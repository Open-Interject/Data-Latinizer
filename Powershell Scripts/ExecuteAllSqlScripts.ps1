# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# 
#  NOTE: In order to run Powershell scripts, you need to enable them... There is a security vulnerability if web-based scripts are downloaded and executed
#  ...   if you see... MyFileName.ps1 cannot be loaded because running scripts is disabled on this system. For more information, see about_Execution_Policies at http://go.microsoft.com/fwlink/?LinkID=135170.
#  ... Open PowerShell
#  ...		To see current execution policy:
#  ...           Get-ExecutionPolicy   
#  ...		To change execution policy for current powershell session: 
#  ...           Set-ExecutionPolicy -Scope Process -ExecutionPolicy ByPass -Force
#  ...		IMPORTANT: This will only affect the execution policy for you current powershell session. 
#
#  NOTE: If you clone this repository using git then you should be able to skip the above step. The above error only happens when you download the .zip file of this repository
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

### HOW TO USE ###
# In Command Prompt:
# 	powershell.exe -File "FolderPathToThisScript\ExecuteSqlScripts.ps1" ParentFolder ServerInstance Database [Username] [Password]
#
# In Powershell:
#	.\ExecuteSqlScripts.ps1 ParentFolder ServerInstance Database [Username] [Password]
#
# Script Requirements: invoke-sqlcmd
#
### Parameters ###
# $Argument1 = ParentFolder 	-- the folder that contains SQL Object folders e.g. "Schema", "Table" ...
# $Argument2 = ServerInstance	-- Server to connect to
# $Argument3 = Database			-- Database to script SQL files to
# $Argument4 = Username			-- username of server
# $Argument5 = Password			-- password of server
param($Argument1,$Argument2,$Argument3,$Argument4,$Argument5)

$ParentFolder 	= $Argument1
$ServerInstance = $Argument2
$Database		= $Argument3
$Username		= $Argument4
$Password		= $Argument5

# default null username/password to empty string
if ($Username -eq $null) {$Username = ""}
if ($Password -eq $null) {$Password = ""}

# ordered array of child folders in ParentFolder to iterate through
$FolderArray = "Schema", "Table", "TableData", "View", "UserDefinedFunction", "StoredProcedure"

# iterate through child folders
foreach ($child in $FolderArray) {
	"Running $child SQL Scripts ..."
	
	# get files in folder
	$files = Get-ChildItem "$ParentFolder\$child\"
	
	# loop through sql scripts
	for ($i=0; $i -lt $files.Count; $i++) {
		if ($files[$i].FullName -notlike "*.sql") {continue;} # skip files that are not sql scripts
		
		$SQLFile = $files[$i]
		"	$SQLFile ..." # print out what file we are executing
		if ($Username -ne "" -and $Password -ne "") { # pass username and passowrd to sql
			invoke-sqlcmd -inputfile $files[$i].FullName -serverinstance $ServerInstance -database $Database -username $Username -password $Password
		}
		else {
			invoke-sqlcmd -inputfile $files[$i].FullName -serverinstance $ServerInstance -database $Database
		}
	}
}