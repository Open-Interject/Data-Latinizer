# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# 
#  NOTE  In order to run Powershell scripts, you need to enable them... There is a security vulnerability if web-based scripts are downloaded and executed
#  ... if you see... MyFileName.ps1 cannot be loaded because running scripts is disabled on this system. For more information, see about_Execution_Policies at http://go.microsoft.com/fwlink/?LinkID=135170.
#  ... search for PowerShell, R-Click, Run As Administrator
#  ...           Get-ExecutionPolicy   
#  ...           set-executionpolicy -scope CurrentUser -executionPolicy RemoteSigned
#  ...           Get-ExecutionPolicy   
#  ...           set-executionpolicy -scope CurrentUser -executionPolicy Restricted
#  set-executionpolicy -scope CurrentUser -executionPolicy RemoteSigned
#
#            1) Find "Windows PowerShell ISE" as a program
#            2) Run as administrator
#            3) Run from a new window, not a saved file.... file based scripts are considered dangerous, so they can''t run in some configurations
#
#  # To see what version of powershell you have, run this line:
#  $PSVersionTable.PSVersion
# 
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

   
# EXAMPLES: HOW TO EXECUTE THIS SCRIPT
# call this script by running command prompt as administrator
# Use one of these example commands. Command arguments are separated by spaces and kept on a single line
#
#       powershell.exe -File 
#           "Full-Path-To-This-Powerhsell-Script"               # powershell file 
#           "Server-Name"                                       # arg 1
#           "Database-Name"                                     # arg 2
#           "Path-To-Final-File-Location"                       # arg 3
#           True/False   -DELETE ROOT FOLDER before scripting   # arg 4
#           True/False   -UseSchemaBasedFolderStructure         # arg 5
#           True/False   -SquareBracketFileNames                # arg 6
#           "SqlServer-Username"                                # arg 7
#           "SqlServer-Password"                                # arg 8
#			"Comma Delimited List of Schemas"					# arg 9
#			True/False 	 -Script Table Data						# arg 10
#
#.......minimal use 
#       powershell.exe -File "D:\Powershell_Scripts\GenerateSqlScripts.ps1" ".\SQLEXPRESS" "Sandbox" "D:\Temp"    
#
#.......delete the root folder before scripting database
#       powershell.exe -File "D:\Powershell_Scripts\GenerateSqlScripts.ps1" ".\SQLEXPRESS" "Sandbox" "D:\Temp" true   
#
#.......store files by schema, then type, (not just type)
#       powershell.exe -File "D:\Powershell_Scripts\GenerateSqlScripts.ps1" ".\SQLEXPRESS" "Sandbox" "D:\Temp" false true   
#
#.......store files by object type, (ignore Schema in folder structure)
#       powershell.exe -File "D:\Powershell_Scripts\GenerateSqlScripts.ps1" ".\SQLEXPRESS" "Sandbox" "D:\Temp" false false   
#
#.......specifically use square brackets in file names
#       powershell.exe -File "D:\Powershell_Scripts\GenerateSqlScripts.ps1" ".\SQLEXPRESS" "Sandbox" "D:\Temp" false false true   
#
#.......specifically remove square brackets in file names
#       powershell.exe -File "D:\Powershell_Scripts\GenerateSqlScripts.ps1" ".\SQLEXPRESS" "Sandbox" "D:\Temp" false false false   
#
#.......use SQL Server Authentication (eg. to Azure in this example)
#       powershell.exe -File "D:\Powershell_Scripts\GenerateSqlScripts.ps1" "tcp:abcde12345.database.windows.net,1433" "Sandbox" "D:\Temp" false false true "ThisIsAFakeUsername" "ThisIsAFakePassword"   
#
#.......store files for specific schemas
#		powershell.exe -File "D:\Powershell_Scripts\GenerateSqlScripts.ps1" ".\SQLEXPRESS" "Sandbox" "D:\Temp" false false false "" "" "dbo,someschema"
#
#.......store files and table data 
#		powershell.exe -File "D:\Powershell_Scripts\GenerateSqlScripts.ps1" ".\SQLEXPRESS" "Sandbox" "D:\Temp" false false false "" "" "" true



#==========================================================================
#====== Code Step 1 of 6... pull the command arguments from when this script was called, and log them in the output
#==========================================================================

# Wire up input parameters to this Powershell Script
param($Argument1,$Argument2,$Argument3,$Argument4,$Argument5,$Argument6,$Argument7,$Argument8, $Argument9, $Argument10)


if($Argument1 -eq $null){$Argument1 = ""} # server
if($Argument2 -eq $null){$Argument2 = ""} # dbname
if($Argument3 -eq $null){$Argument3 = ""} # FileLocation
if(!($Argument3.EndsWith("\"))) { $Argument3 = $Argument3 + "\"}# Make sure there''s a trailing backslash, used later in the file name.
if($Argument4 -eq $null){$Argument4 = "False"} #DELETE ROOT FOLDER... Should the root folder be completely deleted before scipting?
if($Argument5 -eq $null){$Argument5 = "False"} #UseSchemaFolders... Store items based on Schema then Type... root/dbo/Table   root/dbo/View
if($Argument6 -eq $null){$Argument6 = "False"} #Use Square Brackets In File Names ? (default is false, as before)
if($Argument7 -eq $null){$Argument7 = ""} # Username
if($Argument8 -eq $null){$Argument8 = ""} # Password
if($Argument9 -eq $null){$Argument9 = ""} # comma delimited string of schemas. leave blank for all schemas 
if($Argument10 -eq $null){$Argument10 = "False"} # script out table data


$server              = $Argument1
$dbname              = $Argument2
$FileLocation        = $Argument3
$DeleteRootFolder    = ($Argument4 -eq "True")
$UseSchemaFolders    = ($Argument5 -eq "True")
$UseSquareBrackets   = ($Argument6 -eq "True")
$username            = $Argument7
$password            = $Argument8
$schemas			 = $Argument9
$ScriptTableData	 = ($Argument10 -eq "True")

cls # clear screen

""
"Instance is $server" #                 server
"Database is $dbname" #                 dbname
"Output Location is $FileLocation" #    FileLocation
"" 


#==========================================================================
#====== Code Step 2 of 6... Resolve Connection to Database and SMO object
#==========================================================================

$isAzure = ($server.Contains(".database.windows.net"))# This would be an AZURE database
if($isAzure -eq $true){"This is an Azure db."}

#Add a reference to the SQL SDK API 
[System.Reflection.Assembly]::LoadWithPartialName("Microsoft.SqlServer.SMO") | out-null

#Get the server itself
if(($username.Length -gt 0) -and ($password.Length -gt 0))
{
    "Using Connection String to connect to Database..."
    $TrustedConnectionSegment =""
    if($isAzure) 
    {
        $TrustedConnectionSegment = "Trusted_Connection=False;"
    }
    $connection = New-Object System.Data.SqlClient.SqlConnection
    $connection.ConnectionString = "Server=$server;  Database=$dbname;  User ID=$username;  Password=$password;  $TrustedConnectionSegment  Encrypt=True;  Connection Timeout=30;"

    $SMOserver = New-Object ("Microsoft.SqlServer.Management.Smo.Server") $connection
}
else
{
    "Using Windows Login to connect to Database..."
    #Uses Windows Credentials
    $SMOserver = New-Object ("Microsoft.SqlServer.Management.Smo.Server") -argumentlist $server
}

# original default used Windows Login....  $SMOserver = New-Object (''Microsoft.SqlServer.Management.Smo.Server'') -argumentlist $server
if($SMOserver.InstanceName -eq $null)
{
    "No SQL server was found with the name ''$server''"
    return
}

"Successfully opened server ''$server''"

#Get the database on the server
$db = $SMOserver.databases[$dbname]
if($db -eq $null)
{
    "No database was found on the server with the name ''$dbname''"
    return
}

"Successfully opened database ''$dbname''"



#==========================================================================
#====== Code Step 3 of 6... Resolve File System and Folders
#==========================================================================

#Build this portion of the directory structure out here in case scripting takes more than one minute.
$DateFolder = "" #get-date -format yyyyMMddHHmm
$RootPath = $FileLocation + $($dbname) + "\" + $DateFolder
    
""
"Server is set as $server [$dbname] and will be saved to $RootPath"
""

if($DeleteRootFolder -eq $true)
{
    "Deleting existing folders"
    if (( Test-Path -path "$RootPath" )) # Remove folder if existing
    {
        Remove-Item -Recurse -Force $RootPath 
    }
}    

if (!( Test-Path -path "$RootPath" )) # create it if not existing
{
    $progress ="attempting to create directory $RootPath"
    Try 
    { 
        New-Item "$RootPath" -type directory | out-null 
    }
    Catch [system.exception]
    {
        Write-Error "error while $progress. $_"
        return
    }
}
    



#==========================================================================
#====== Code Step 4 of 6... Resolve Scripting Options
#==========================================================================
"Preparing Scripting options"

# Set the list option for CREATE-IfNotExists for Tables
$IfNotExistsOptions = new-object ("Microsoft.SqlServer.Management.Smo.Scripter") ($SMOserver)
# a full list of options is found here: https://msdn.microsoft.com/en-us/library/microsoft.sqlserver.management.smo.scriptingoptions.aspx
$IfNotExistsOptions.Options.AppendToFile = $False
$IfNotExistsOptions.Options.AllowSystemObjects = $False
$IfNotExistsOptions.Options.ClusteredIndexes = $True
$IfNotExistsOptions.Options.DriAll = $True
$IfNotExistsOptions.Options.ScriptDrops = $False
$IfNotExistsOptions.Options.IncludeHeaders = $False
$IfNotExistsOptions.Options.ToFileOnly = $True
$IfNotExistsOptions.Options.Indexes = $True
$IfNotExistsOptions.Options.Permissions = $True
$IfNotExistsOptions.Options.WithDependencies = $False
$IfNotExistsOptions.Options.IncludeIfNotExists = $True # For tables/views, the script should not just create
$IfNotExistsOptions.Options.AnsiFile = $True
# $IfNotExistsOptions.Options.UseDataBase = $True
$IfNotExistsOptions.Options.NoCollation = $True



# set up the script options for the DROP + CREATE of other objects
# a full list of options is found here: https://msdn.microsoft.com/en-us/library/microsoft.sqlserver.management.smo.scriptingoptions.aspx
$DropOptions = new-object ("Microsoft.SqlServer.Management.Smo.Scripter") ($SMOserver)
$DropOptions.Options.AppendToFile = $False # does not append, this is a new file
$DropOptions.Options.AllowSystemObjects = $False
$DropOptions.Options.ClusteredIndexes = $True
$DropOptions.Options.DriAll = $True
$DropOptions.Options.ScriptDrops = $True
$DropOptions.Options.IncludeHeaders = $False
$DropOptions.Options.ToFileOnly = $True
$DropOptions.Options.Indexes = $True
$DropOptions.Options.WithDependencies = $False
$DropOptions.Options.IncludeIfNotExists = $True
$DropOptions.Options.AnsiFile = $True
$DropOptions.Options.NoCollation = $True

#Create Again
$CreateOptions = new-object ("Microsoft.SqlServer.Management.Smo.Scripter") ($SMOserver)
$CreateOptions.Options.AppendToFile = $True # This appends to the DROP script file
$CreateOptions.Options.AllowSystemObjects = $False
$CreateOptions.Options.ClusteredIndexes = $True
$CreateOptions.Options.DriAll = $True
$CreateOptions.Options.ScriptDrops = $False
$CreateOptions.Options.IncludeHeaders = $False
$CreateOptions.Options.ToFileOnly = $True
$CreateOptions.Options.Indexes = $True
$CreateOptions.Options.Permissions = $True
$CreateOptions.Options.WithDependencies = $False
$CreateOptions.Options.AnsiFile = $True
$CreateOptions.Options.NoCollation = $True
    


#==========================================================================
#====== Code Step 5 of 6... Get List of Database Objects
#==========================================================================
"Looking up database objects"
#Objects to script and save to file:
$Objects = @() # empty array
$Objects += $db.StoredProcedures
$Objects += $db.Views
$Objects += $db.Tables
$Objects += $db.ApplicationRoles
#$Objects += $db.Assemblies
#$Objects += $db.ExtendedStoredProcedures
#if ($isAzure -eq $false) { $Objects += $db.ExtendedProperties }
#$Objects += $db.PartitionFunctions
#$Objects += $db.PartitionSchemes
$Objects += $db.Roles
$Objects += $db.Rules
$Objects += $db.Schemas
#$Objects += $db.Synonyms
#$Objects += $db.Triggers
#$Objects += $db.UserDefinedAggregates
#$Objects += $db.UserDefinedDataTypes
$Objects += $db.UserDefinedFunctions
#$Objects += $db.UserDefinedTableTypes
#$Objects += $db.UserDefinedTypes
$Objects += $db.Users

""
""
#==========================================================================
#====== Code Step 6 of 6... Loop Over Objects - Generate Scripts
#==========================================================================
"Looping Over Objects"    
foreach ($ScriptThis in $Objects) #  | where {!($_.IsSystemObject)}) 
{
    if ($ScriptThis.IsSystemObject -eq $true){continue}



    $ObjectType = $ScriptThis.GetType().Name
    $Schema = $ScriptThis.Schema
    If ($Schema -eq $null){$Schema = ""} 
    $ObjectName = $ScriptThis.Name
	
	# skip object if not in list of schemas to script
	if ($schemas -ne "" -and $schemas -split ',' -notcontains $Schema)
	{
		continue
	}

    #====== Decide Which scripting option to use in this case.
    if($ObjectType -eq "Table" -or $ObjectType -eq "View")
    { 
        $CreateIfNotExists = $True 
    }
    else
    {
        $CreateIfNotExists = $false # DropAndCreate
    }


    #====== Decide which folder structure to use
    if($UseSchemaFolders -eq $true -and $Schema -ne "") # if Schema is missing (as with Users and Roles) then use the same root folder strutctre
    {
        # Group all folders first based on Schema, and then type
        $Folder = "$RootPath\_$Schema\$ObjectType"
    }
    else
    {
        $Folder = "$RootPath\$ObjectType" # Group folder just based on type
    }


    #====== Decide which file Name structure to use
    if ($UseSquareBrackets -and $Schema -ne "") # if Schema is missing (as with Users and Roles) then don''t use square brackets anyhow
    {
        $FileName = "[$Schema].[$ObjectName]"            
    }
    else
    {
        $FileName = $ScriptThis -replace "\[|\]" ###### Remove all square brackets in file names
    }


    #====== Build folder structures.  Remove the date folder if you want to overwrite.
    if ((Test-Path -Path $Folder) -eq $false) # eg... c:\MyFolder\StoredProcedure   
    {
        #Create the folder for "StoredProcedure" to hold scripted files.
        "Creating Folder $TypeFolder"
        [system.io.directory]::CreateDirectory($Folder)
        #new-item -type directory -name "$TypeFolder" -path "$RootPath" | Out-Null
    }
        

    "Scripting $ObjectType $ScriptThis"
    #====== SCRIPT the object, this is where each object actually gets scripted one at a time.
    If($CreateIfNotExists -eq $true)
    {
		$IfNotExistsOptions.Options.ScriptData = $False
		$IfNotExistsOptions.Options.ScriptSchema = $True;
        $IfNotExistsOptions.Options.FileName = "$Folder\$FileName.SQL"
        $IfNotExistsOptions.Script($ScriptThis) 
    }
    else # DropAndCreate
    {
        $DropOptions.Options.FileName = "$Folder\$FileName.SQL"
        $CreateOptions.Options.FileName = "$Folder\$FileName.SQL"
        $DropOptions.Script($ScriptThis) 
        $CreateOptions.Script($ScriptThis) 
    }
	
	
	#===== SCRIPT Data for objects that have data.
	if ($ScriptTableData -eq $True -and $ObjectType -eq "Table")
	{
		"Scripting $ObjectType Data $ScriptThis"
		
		$Folder = $Folder + "Data"
		
		if ((Test-Path -Path $Folder) -eq $false) # eg... c:\MyFolder\TableData   
		{
			#Create the folder to hold scripted files.
			"Creating Folder $Folder"
			[system.io.directory]::CreateDirectory($Folder)
		}
	
		$IfNotExistsOptions.Options.ScriptData = $True
		$IfNotExistsOptions.Options.ScriptSchema = $False; # will not script out create table statements
		$IfNotExistsOptions.Options.FileName = "$Folder\$FileName.SQL"
		$IfNotExistsOptions.EnumScript($ScriptThis)
	}
    
} #This ends the loop
        
    