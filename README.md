## Data-Latinizer Readme

#### How to Load Data-Latinizer Into Your Database 
1. Download the .zip file or clone to your local machine
1. Open Powershell and navigate ([cd](https://docs.microsoft.com/en-us/powershell/scripting/getting-started/cookbooks/managing-current-location?view=powershell-5.1)) to 'FullFolderPath\Data-Latinizer\Powershell Scripts'
1. Run the following command:
`.\ExecuteAllSqlScripts.ps1 "FullFolderPath\Data-Latinizer\" "YourServerInstance" "YourDatabase"`

The `.\ExecuteAllSqlScripts.ps1` Powershell script will loop through the folders and execute the SQL files on the specified ServerInstance and Database.

If you see an error along the lines of:
```
MyFileName.ps1 cannot be loaded because running scripts is disabled on this system. For more information, see about_Execution_Policies at http://go.microsoft.com/fwlink/?LinkID=135170.
```

Then you must first run the following:
```
Set-ExecutionPolicy -Scope Process -ExecutionPolicy ByPass -Force
```
The above code will change your execution policy for your current powershell session to allow you to run downloaded scripts.